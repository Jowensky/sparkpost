const express = require('express');
const app  = express();
const cors = require('cors');
const pool = require("./db");

//middleware
app.use(cors());
app.use(express.json()); // req.body

// base path && get all associates
app.get("/sparkpost", async(req, res) => {
    try {
        const allAssociates = await pool.query("SELECT * FROM associate")
        res.json(allAssociates.rows)
    } catch (err) {
        console.error(err.message)
    }
});

// get an associate
app.get("/sparkpost/:name", async(req, res) => {
    try {
        const { name } = req.params;
        const associate  = await pool.query(
            "SELECT * FROM associate WHERE name = $1",
            [name]
        );
        res.send(associate.rows)
    } catch (err) {
        console.error(err.message)
    }
});

// create an associate
app.post("/sparkpost", async(req, res) => {
    try {
        const { name, age } = req.body;
        const newAssociate = await pool.query(
            "INSERT INTO associate (name, age) VALUES($1, $2) RETURNING *",
            [name, age]
        );
        res.json(newAssociate.rows);
    } catch (err) {
        console.error(err.message);
    }
});

//update an associate
app.put("/sparkpost", async(req, res) => {
    try {
        const { age, name } = req.body;
        const updateAssociate = await pool.query(
            "UPDATE associate SET age = $1 WHERE name = $2",
            [age, name]
        );
        res.json("Associate table was updated!")
    } catch (err) {
        console.error(err.message)
    }
});


const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));