CREATE DATABASE sparkpost;

CREATE TABLE associate(
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    age INT
);